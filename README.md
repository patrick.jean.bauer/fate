<div align=center>

# Fate Core & Accelerated

<img title="Minimum core version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/foundry-projects/fate/-/raw/master/src/system.json&label=core&query=minimumCoreVersion&suffix=%2B&style=flat-square&color=important">
<img title="Latest compatible version" src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/foundry-projects/fate/-/raw/master/src/system.json&label=compatible&query=compatibleCoreVersion&style=flat-square&color=important">
<img src="https://img.shields.io/badge/dynamic/json?url=https://gitlab.com/foundry-projects/fate/-/raw/master/package.json&label=version&query=version&style=flat-square&color=success">

[![Roadmap](https://img.shields.io/badge/roadmap-trello-blue?style=flat-square&logo=trello)](https://trello.com/b/vWFk8aWr/nick-east-foundry-vtt-projects)
[![Chat on Discord](https://img.shields.io/discord/520640779534729226?style=flat-square&label=discord&logo=discord)](https://discord.gg/59Tz2X7)
[![Twitter Follow](https://img.shields.io/badge/follow-%40NickEastNL-blue.svg?style=flat-square&logo=twitter)](https://twitter.com/NickEastNL)
[![Become a Patron](https://img.shields.io/badge/support-patreon-orange.svg?style=flat-square&logo=patreon)](https://www.patreon.com/nick_east)
[![Donate via Ko-Fi](https://img.shields.io/badge/support-ko--fi-ff4646?style=flat-square&logo=ko-fi)](https://ko-fi.com/nickeast)

</div>

This is an implementation of the **Fate Core** and its subsystem **Fate Accelerated** (FAE) for the Foundry VTT. The system contains both the Core and FAE sheets as separate Actor types.

## Features

List of features that are currently functional, as well as those that are planned to be added.

### Current

-   Fate Accelerated character sheet
-   "Stunt" item type

### Planned

-   Fate Core character sheet
-   (Core) "Skill" item type
-   Make rolls using chosen Skill (Core) or Approach (FAE) values
-   Compendium for default/example Stunts and Skills

### Available languages

-   English
-   German

## Support

All the work that I do for Foundry I do in my spare time. If you love using it, please consider supporting me through either [Patreon](https://www.patreon.com/nick_east) or [Ko-Fi](https://ko-fi.com/nickeast) so that I can continue my work and worry less about life itself.
